
from  Product import *


option = 0

productService =  ProductService()

while option != 5:
  
    print "+----------------------------+"
    print "|         Menu               |"
    print "+----------------------------+"
    print "| 1 - Cadastrar Produto      |"
    print "| 2 - Editar Produto         |"
    print "| 3 - Deletar Produto        |"
    print "| 4 - Relatorio de Produtos  |"
    print "| 5 - Sair                   |"
    print "------------------------------"

    option = input('Escolha a opcao: ')

    if option == 1:
        print "Cadastro de Produto"
        productNew = {}
        productNew['code']   = input('Informe o codigo  :')
        productNew['title']  = raw_input('Informe o title :')
        productNew['price']  = raw_input('Informe o preco   :')
        productNew['qtd']    = raw_input('Informe a quantidade   :')

        productService.saveProduct(productNew)

        print "Cadastrado com sucesso !"
 
    elif option == 4 :
        productService.generateReport()
    elif option == 2:
        coderead = raw_input('Informe o codigo  :')
        isUpdate = productService.existProductByCode(
            coderead
        )

        if isUpdate:
            print "Produto Encotrado !"
            product = productService.getProductByCode(coderead)
            product.setTitle(
                raw_input('Informe o title :')
            )
            product.setPrice(
                raw_input('Informe o preco   :')
            )
            product.setQtd(
                raw_input('Informe a quantidade   :')
            )
            productService.updateProduct(product)
        else:
            print "Nenhum Produto Encotrado !"


    else :
        print "Opcao invalida !"
   
else :
  print "Obrigado por utilizar o sistema !" 


