import sys

class Product(object):
    """
        Class Entity Product
        @author Augusto Berwaldt  <augusto.berwaldt@gmail.com>
    """

    def __init__(self, code, title, price, qtd):
        self.code = code
        self.title = title
        self.price = price
        self.qtd = qtd

    def getCode(self):
        return self.code

    def getTitle(self):
        return self.title

    def getPrice(self):
        return self.price

    def getQtd(self):
        return self.qtd

    def setCode(self, code):
         self.code = code

    def setTitle(self, title):
         self.title =  title

    def setPrice(self, price):
        self.price = price

    def setQtd(self, qtd):
        self.qtd = qtd


class ProductService(object):
    """
        Class  ProductService
        @author Augusto Berwaldt  <augusto.berwaldt@gmail.com>
    """

    products = {}

    def saveProduct(self, product):
        self.products[product['code']] = Product(product['code'], product['title'], product['price'], product['qtd'])

    def updateProduct(self, product):
        self.products[product['code']] = product

    def removeProduct(self, code):
        try:
            del self.products[code]
        except KeyError:
            pass


    def generateReport(self):

        if len(self.products) > 0 :
            fwrite = open("./report.csv", "w")
            for item in self.products:
                fwrite.write(str(item.getCode())  + ";")
                fwrite.write(str(item.getTitle()) + ";")
                fwrite.write(str(item.getPrice()) + ";")
                fwrite.write(str(item.getQtd())   + ";")

            fwrite.close()

    def existProductByCode(self, code):
        for index,item in self.products.iteritems():
            if item.getCode() == code:
                return True

        return False

    def getProductByCode(self, code):
        return self.products[code]
